let React = window.React = require('react');
let reactRender = require('-aek/react/utils/react-render');
let { VBox, CBox } = require('-components/layout');
const {BasicSegment} = require("-components/segment");
let Container = require('-components/container');
const ReactJsBarcode = require("./components/jsbarcode-react");
const request = require("-aek/request");
var Listview = require("@ombiel/aek-lib/react/components/listview");

const Config = require('./modules/config');

let Screen = React.createClass({

  getInitialState(){
    return {
      loading: true
    };
  },

  componentDidMount(){
    this.getData();
  },

  getData(){
    request.action("get-studentid").end((err,res)=>{
      if(res && !err && res.body){
        this.setState({data:res.body, loading:false, error:false});
      }else{
        this.setState({data:null, loading:false, error:true});
      }
    });
  },

  render:function() {
    let data = this.state.data;
    let photo, barcode, studentName, top;
    let items = [];
    if (data && !this.state.loading){
      if(data.barcode){
        barcode =
          <BasicSegment nopadding textAlign="center" className="barcode"> <ReactJsBarcode content={ data.barcode } options={{ format:'CODE128', displayValue:false, height:35 }} /> </BasicSegment>;
      }

      if(data.photo){
        photo = <div className="photo"><img src={data.photo} alt="Photo"/></div>;
      }

      if(data.studentName && data.surname){
        studentName = <h3 className="studentname">
          {data.studentName + " " + data.surname}
        </h3>;
      }

      top = <BasicSegment className="top" textAlign="center">
        {photo}
        {studentName}
        {barcode}
      </BasicSegment>;


      if(data.id){
        items.push({heading:Config.language.idLabel, text:data.id});
      }

      if(data.schoolYear){
        items.push({heading:Config.language.schoolYearLabel, text:data.schoolYear});
      }
    }

    return (
      <Container>
        <BasicSegment nopadding={true} loading={this.state.loading} style={{height:"100%"}}>
          <VBox>
            <CBox data-flex={2}>
              <BasicSegment nopadding={true} className="background" style={{backgroundImage:`url(${Config.background})`}}>
                  {top}
                  <BasicSegment>
                  <Listview items={items} flush style={{"background":"none"}}/>
                </BasicSegment>
                <div className="logo1"><img src={Config.logo1} alt="logo1"/></div>
                <div className="logo2"><img src={Config.logo2} alt="logo2"/></div>
              </BasicSegment>
            </CBox>
          </VBox>
        </BasicSegment>
      </Container>
    );
  }
});

reactRender(<Screen />);
