var React = window.React = require("react");
var { ErrorMessage } = require("-components/message");

var JsBarcode = require("jsbarcode");

var ReactJsBarcode = React.createClass({
    getInitialState:function() {
      return { error:false };
    },

    componentDidMount: function(){
      var content = this.props.content;
      var options = this.props.options;
      var renderElement = this.refs.barcode;
      var barcodePromise = new Promise((resolve) => {
        resolve(new JsBarcode(renderElement, content, options));
      });

      barcodePromise.catch((reason) => {
        this.setState({ error:reason });
      });
    },

    render:function() {
      var result = <img id="pass" ref="barcode" style={{ maxWidth:'100%', margin:'0 auto', display:'block' }} />;
      if(this.state.error !== false) {
        result = <ErrorMessage>{ this.state.error }</ErrorMessage>;
      }

      return (
        result
      );
    }
});

module.exports = ReactJsBarcode;
